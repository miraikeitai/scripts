#!/bin/sh

url="https://ja.osdn.net/dl/pukiwiki/update_pukiwiki_150to151_utf8.patch.zip"
fileName="update_pukiwiki_150to151_utf8.patch"

curl -o ${fileName}.zip -L ${url}
unzip ${fileName}.zip
patch -p1 < ${fileName}
