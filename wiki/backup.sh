#!/bin/bash

cd $(dirname $0)
git add -A
git commit -m "Commit at $(date "+%Y-%m-%d %T")" || true
git push origin master
