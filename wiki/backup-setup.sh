#!/bin/bash

# crontab setting for backup
dir=$(cd $(dirname $0) && pwd)
curl -o ${dir}/backup.sh -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/backup.sh
chmod +x ${dir}/backup.sh
echo "backup.sh" >> .gitignore
(crontab -l; echo "1 * * * * ${dir}/backup.sh") | crontab -
