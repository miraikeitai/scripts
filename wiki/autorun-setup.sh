#!/bin/sh

dir=$(cd $(dirname $0) && pwd)
name=run-wiki-server.sh
file=/var/lib/cloud/scripts/per-boot/$name
docker_compose=$(which docker-compose)

sudo chkconfig cloud-init on
cat << EOF > $dir/$name
#!/bin/sh
cd $dir
$docker_compose stop
sudo -u $USER $docker_compose up -d
EOF
sudo mv $dir/$name $file
sudo chmod +x $file
