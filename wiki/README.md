# Wiki setup scripts

## 新規にWikiを構築する

まずはローカルで以下を実行し年度を入力する。

```
curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/create-new-wiki.sh | sh
```

フォルダが作られるのでGitLabにレポジトリを作ったり、してプッシュしておく

## WikiをAWS EC2のAmazonLinuxで動かす

AWS EC2の **AmazonLinux** を使用する場合のWikiの環境構築手順を以下に示す。

1. はじめに初期設定用のスクリプトを実行
    - `curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/initial-setup.sh | sh`
1. 一度SSHをログアウトし再度ログイン
    - `exit` and `ssh`
1. 使用するWikiのGitLabリポジトリに表示されたSSH Public Keyを登録
    - [公開鍵の登録方法](https://gitlab.com/miraikeitai/scripts/wikis/%E5%85%AC%E9%96%8B%E9%8D%B5%E3%81%AE%E7%99%BB%E9%8C%B2%E6%96%B9%E6%B3%95)
1. miraikeitai wikiをSSHを使ってクローン
    - `git clone git@gitlab.com:miraikeitai/wiki20xx.git`
1. クローンしたリポジトリのディレクトリに移動
    - `cd wiki20xx`
1. wikiサーバーを起動
    - `docker-compose up -d`
1. 自動バックアップのセットアップスクリプトを実行
    - `curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/backup-setup.sh | sh`
1. 自動起動のセットアップスクリプトを実行
    - `curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/autorun-setup.sh | sh`

## PukiWikiをアップグレードする方法

### 1.4.7 から 1.5.0 にアップグレード

```
curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/upgrade_147to150.sh | sh
```

### 1.5.0 から 1.5.1 にアップグレード

```
curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/upgrade_150to151.sh | sh
```

## テンプレートファイル

新規にPukiWikiを構築する際のテンプレートファイル

### docker-compose.yml

```
curl -o docker-compose.yml -L https://gitlab.com/miraikeitai/scripts/raw/master/wiki/template/docker-compose.yml
```

### .gitignore

```
curl -o .gitignore -L https://gitlab.com/miraikeitai/scripts/raw/master/wiki/template/.gitignore
```
