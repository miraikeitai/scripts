#!/bin/bash

# Update yum packages and install
sudo yum update -y
sudo yum install -y \
  git \
  docker \
  tzdata

# Set timezone to Tokyo
sudo ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

# Setting for Git
git config --global core.editor 'vim -c "set fenc=utf-8"'

# Setting for Dcoker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# SSH setting for git clone and push
ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ""
cat << EOF

------------ SSH Public Key ------------

$(cat ~/.ssh/id_rsa.pub)

----------------------------------------

EOF
cat << EOF
1. 一度SSHをログアウトし再度ログイン
    (\`exit\` and \`ssh\`)
2. 使用するWikiのGitLabリポジトリに表示されたSSH Public Keyを登録
    (Allow Write and Read)
3. miraikeitai wikiをSSHを使ってクローン
    (\`git clone git@gitlab.com:miraikeitai/wiki20xx.git\`)
4. クローンしたリポジトリのディレクトリに移動
    (\`cd wiki20xx\`)
5. Wikiサーバーを起動
    (\`docker-compose up -d\`)
6. 自動バックアップのセットアップスクリプトを実行
    (\`curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/backup-setup.sh | sh\`)
7. 自動起動のセットアップスクリプトを実行
    (\`curl -sL https://gitlab.com/miraikeitai/scripts/raw/master/wiki/autorun-setup.sh | sh\`)

EOF

exit
