#!/bin/sh

url="http://jaist.dl.osdn.jp/pukiwiki/61634/update_pukiwiki_147to150_utf8.patch"
fileName="update_pukiwiki_147to150_utf8.patch"

curl -o ${fileName} -L ${url}
patch -p1 < ${fileName}
