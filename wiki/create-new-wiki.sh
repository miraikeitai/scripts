#!/bin/sh

pukiwiki=pukiwiki-1.5.1_utf8
dir=$(cd $(dirname $0) && pwd)

echo "年度を入力 (例:2017) : "
while read year; do
    case "$year" in
       20[12][0-9]) echo " $year 年度用のwikiを作成します" && break ;;
       *) echo "2010~2029を入力してください" ;;
   esac
done

wiki_home=$dir/wiki$year
mkdir -p $wiki_home && cd $wiki_home

if [ ! -s $wiki_home/html ]; then
    curl -sL "https://ja.osdn.net/frs/redir.php?m=ymu&f=pukiwiki%2F64807%2F$pukiwiki.zip" -o $pukiwiki.zip
    unzip $pukiwiki.zip 1> /dev/null
    mv $pukiwiki html
    sudo chmod -R 777 html
fi

if [ -s $wiki_home/$pukiwiki.zip ]; then
    rm -f $wiki_home/$pukiwiki.zip
fi

if [ ! -s $wiki_home/docker-compose.yml ]; then
    curl -o docker-compose.yml -L https://gitlab.com/miraikeitai/scripts/raw/master/wiki/template/docker-compose.yml
fi

if [ ! -s $wiki_home/.gitignore ]; then
    curl -o .gitignore -L https://gitlab.com/miraikeitai/scripts/raw/master/wiki/template/.gitignore
fi
